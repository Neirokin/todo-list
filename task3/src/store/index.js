import Vue from 'vue'
import Vuex from 'vuex'
// import axios from 'axios'
import taskListModule from './modules/taskListModule.js'
import taskModule from './modules/taskModule.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    taskListModule,
    taskModule
  },
  // getters: {
  //   COMPLETED_TASKLISTS(state, getters) {
  //     console.log(state);
  //     let result = [];
  //     let taskLists = getters['taskListModule/ALL_TASKLISTS'];
  //     for(let taskList of taskLists) {
  //       let tasks = getters['taskModule/TASKS_IN_TASKLIST'](taskList.id);
  //       let completeTasksCounter = 0;
  //       if(tasks.length > 0) {
  //         for(let task of tasks) {
  //           if(task.checked)
  //           { 
  //             completeTasksCounter++;  
  //           }
  //           else{
  //             break;
  //           }
  //         }
  //         if(completeTasksCounter == tasks.length) {
  //           result.push(taskList);
  //         }
  //       }
  //     }
  //     result.sort((a, b) => a.name > b.name ? 1 : -1);
  //     return result;
  //   },
  //   UNCOMPLETED_TASKLISTS(state, getters) {
  //     console.log(state);
  //     let result = [];
  //     let taskLists = getters['taskListModule/ALL_TASKLISTS'];
  //     for(let taskList of taskLists) {
  //       let tasks = getters['taskModule/TASKS_IN_TASKLIST'](taskList.id);
  //       if(tasks.length > 0) {
  //         for(let task of tasks) {
  //           if(!task.checked)
  //           { 
  //             result.push(taskList);
  //             break;  
  //           }
  //         }
  //       }
  //       else {
  //         result.push(taskList);
  //       }
  //     }
  //     result.sort((a, b) => a.name > b.name ? 1 : -1);
  //     return result;
  //   }
  // }
})
