import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import taskListModule from './modules/taskListModule.js'
import taskModule from './modules/taskModule.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    taskLists: [],
    tasks: [],
    baseUrl: "http://localhost:3000/tasklists",
    modules: {
      taskListModule,
      taskModule
    }
  },

  mutations: {
    SET_TASKLISTS_TO_STORE: (state, taskLists) => {
      state.taskLists = taskLists;
    },
    SET_TASKS_TO_STORE: (state, tasks) => {
      state.tasks = tasks;
    },

    ADD_TASKLIST_TO_STORE: (state, taskList) => {
      state.taskLists.push(taskList);
    },
    DELETE_TASKLIST_FROM_STORE: (state, taskListId) => {
      state.taskLists.splice(state.taskLists.indexOf(taskList => taskList.id == taskListId), 1);
    },

    ADD_TASK_TO_TASKLIST: (state, task) => {
      state.tasks.push(task);
    },
    DELETE_TASK_FROM_TASKLIST: (state, taskId) => {
      state.tasks.splice(state.tasks.indexOf(task => task.id == taskId), 1)
    },
    UPDATE_TASK_IN_STORE: (state, taskData) => {
      state.tasks.find(task => task.id == taskData.id).checked = taskData.checked;
    }
  },

  actions: {
    GET_TASKLISTS ({commit}) {
      return axios('http://localhost:3000/tasklists', {
        method: 'GET'
      })
        .then((response) => {
          commit('SET_TASKLISTS_TO_STORE', response.data);
        })
        .catch((err) => {
          console.log(err);
        })
    },
    GET_TASKS ({commit}) {
      return axios('http://localhost:3000/tasks', {
        method: 'GET'
      })
        .then((response) => {
          commit('SET_TASKS_TO_STORE', response.data)
        })
        .catch((err) => {
          console.log(err);
        })
    },

    ADD_TASKLIST ({commit}, taskList) {
      return axios('http://localhost:3000/tasklists', {
        method: 'POST',
        data: taskList
      })
        .then((response) => {
          console.log(response);
          commit('ADD_TASKLIST_TO_STORE', response.data)
        })
        .catch((err) =>{
          console.log(err);
        })
    },
    DELETE_TASKLIST ({commit}, taskListId) {
      return axios(`http://localhost:3000/tasklists/${taskListId}`, {
        method: 'DELETE'
      })
        .then((response) => {
          console.log(response);
          commit('DELETE_TASKLIST_FROM_STORE', taskListId);
        })
        .catch((err) => {
          console.log(err);
        })
    },

    ADD_TASK({commit}, task) {
      return axios(`http://localhost:3000/tasklists/${task.tasklistId}/tasks`, {
        method: 'POST',
        data: task
      })
        .then((response) => {
          console.log(response);
          commit('ADD_TASK_TO_TASKLIST', response.data)
        })
        .catch((err) => {
          console.log(err);
        })
    },
    DELETE_TASK({commit}, taskId) {
      return axios(`http://localhost:3000/tasks/${taskId}`, {
        method: 'DELETE'
      })
        .then((response) => {
          console.log(response);
          commit('DELETE_TASK_FROM_TASKLIST', taskId)
        })
        .catch((err) => {
          console.log(err);
        })
    },
    UPDATE_TASK({commit}, task) {
      return axios(`http://localhost:3000/tasks/${task.id}`, {
        method: 'PATCH',
        data: task
      })
        .then((response) => {
          console.log(response);
          commit('UPDATE_TASK_IN_STORE', response.data);
        })
        .catch((err) => {
          console.log(err);
        })      
    }
  },

  getters: {
    TASKLISTS(state) {
      return state.taskLists;
    },
    
    TASKS(state) {
      return state.tasks;
    },
    TASKS_IN_TASKLIST: state => taskListIndex => {
      return state.tasks.filter(tasks => tasks.tasklistId == taskListIndex);
    }
  }
})
