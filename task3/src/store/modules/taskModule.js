import axios from 'axios'

export default {
    namespaced: true,

    state: {
        tasks: []
    },

    mutations: {
        SET_TASKS_TO_STORE: (state, tasks) => {
            state.tasks = tasks;
            state.tasks.sort((a, b) => a.createdAt < b.createdAt ? 1 : -1);
        },
        ADD_TASK_TO_TASKLIST: (state, task) => {
            state.tasks.push(task);
            state.tasks.sort((a, b) => a.createdAt < b.createdAt ? 1 : -1);
        },
        DELETE_TASK_FROM_TASKLIST: (state, taskId) => {
            state.tasks.splice(state.tasks.findIndex(task => task.id == taskId), 1);
        },
        DELETE_ALL_TASKS_FROM_TASKLIST: (state, taskListId) => {
            let result = state.tasks.filter(task => task.tasklistId != taskListId)
            state.tasks = result;
        },
        UPDATE_TASK_STATUS_IN_STORE: (state, taskData) => {
            state.tasks.find(task => task.id == taskData.id).checked = taskData.checked;
        }
    },

    actions: {
        GET_TASKS ({commit}) {
            return axios('http://localhost:3000/tasks', {
              method: 'GET'
            })
              .then((response) => {
                commit('SET_TASKS_TO_STORE', response.data)
              })
              .catch((err) => {
                console.log(err);
              })
        },
        ADD_TASK({commit}, task) {
        return axios(`http://localhost:3000/tasklists/${task.tasklistId}/tasks`, {
            method: 'POST',
            data: task
        })
            .then((response) => {
            console.log(response);
            commit('ADD_TASK_TO_TASKLIST', response.data)
            })
            .catch((err) => {
            console.log(err);
            })
        },
        DELETE_TASK({commit}, taskId) {
        return axios(`http://localhost:3000/tasks/${taskId}`, {
            method: 'DELETE'
        })
            .then((response) => {
                console.log(response);
                commit('DELETE_TASK_FROM_TASKLIST', taskId)
            })
            .catch((err) => {
                console.log(err);
            })
        },
        DELETE_ALL_TASKS({commit}, taskListId) {
            commit('DELETE_ALL_TASKS_FROM_TASKLIST', taskListId)
        },
        UPDATE_TASK_STATUS({commit}, task) {
        return axios(`http://localhost:3000/tasks/${task.id}`, {
            method: 'PATCH',
            data: task
        })
            .then((response) => {
                console.log(response);
                commit('UPDATE_TASK_STATUS_IN_STORE', response.data);
            })
            .catch((err) => {
                console.log(err);
            })      
        }
    },

    getters: {
        TASKS(state) {
            return state.tasks;
        },
        TASKS_IN_TASKLIST: state => taskListId => {
            return state.tasks.filter(tasks => tasks.tasklistId == taskListId);
        }
        // UNCOMPLETED_TASKS_TASKLIST_ID(state) {
        //     return state.tasks.filter(task => task.checked == false).taskListId;
        // }
    }
}