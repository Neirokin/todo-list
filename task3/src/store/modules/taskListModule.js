import axios from 'axios'
export default {
    namespaced: true,

    state: {
        taskLists: [],
    },

    mutations: {
        SET_TASKLISTS_TO_STORE: (state, taskLists) => {
          state.taskLists = taskLists;
          state.taskLists.sort((a, b) => a.name > b.name ? 1 : -1);
        },
        ADD_TASKLIST_TO_STORE: (state, taskList) => {
          state.taskLists.push(taskList);
          state.taskLists.sort((a, b) => a.name > b.name ? 1 : -1);
        },
        DELETE_TASKLIST_FROM_STORE: (state, taskListId) => {
          state.taskLists.splice(state.taskLists.findIndex(taskList => taskList.id == taskListId), 1);
        },
        UPDATE_TASKLIST_STATUS_IN_STORE: (state, taskListData) => {
          state.taskLists.find(taskList => taskList.id == taskListData.id).status = taskListData.status; 
        }
    },

    actions: {
        GET_TASKLISTS ({commit}) {
            return axios('http://localhost:3000/tasklists', {
              method: 'GET'
            })
              .then((response) => {
                commit('SET_TASKLISTS_TO_STORE', response.data);
              })
              .catch((err) => {
                console.log(err);
              })
        },
        ADD_TASKLIST ({commit}, taskList) {
          return axios('http://localhost:3000/tasklists', {
            method: 'POST',
            data: taskList
          })
            .then((response) => {
              console.log(response);
              commit('ADD_TASKLIST_TO_STORE', response.data)
            })
            .catch((err) =>{
              console.log(err);
            })
        },
        DELETE_TASKLIST ({commit}, taskListId) {
          return axios(`http://localhost:3000/tasklists/${taskListId}`, {
              method: 'DELETE'
          })
            .then((response) => {
              console.log(response);
              commit('DELETE_TASKLIST_FROM_STORE', taskListId);
            })
            .catch((err) => {
                console.log(err);
            })
        },
        UPDATE_TASKLIST_STATUS({commit}, taskList) {
          return axios(`http://localhost:3000/tasklists/${taskList.id}`, {
            method: 'PATCH',
            data: taskList
          })
            .then((response) => {
              console.log(response);
              commit('UPDATE_TASKLIST_STATUS_IN_STORE', taskList);
            })
        }
    },

    getters: {
        ALL_TASKLISTS(state) {
          return state.taskLists.sort((a, b) => a.name > b.name ? 1 : -1);
        },
        COMPLETED_TASKLISTS(state) {
          return state.taskLists.filter(taskList => taskList.status == "completed").sort((a, b) => a.name > b.name ? 1 : -1);
        },
        UNCOMPLETED_TASKLISTS(state) {
          return state.taskLists.filter(taskList => taskList.status == "uncompleted" || taskList.status == "empty").sort((a, b) => a.name > b.name ? 1 : -1);
        }
    }
}