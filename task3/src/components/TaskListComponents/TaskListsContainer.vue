<template>
    <div id="taskListsContainer">
        <div class="row">
            <div class="col-12 taskListsHead px-2">
                <b-form-select v-model="selectedFilter" class="selectFilter" :options="this.options"></b-form-select>
                <hr/>
            </div>
            <div class="col-12 taskListsbody align-self-start">
                <ul class="px-0">
                    <li
                        is="taskList"
                        v-for="taskList in filteredTaskLists"
                        :key="taskList.id"
                        :name="taskList.name"
                        :id="taskList.id"
                        :status="taskList.status"
                        @remove="deleteTaskList(taskList.id, taskList.name)"
                        class="mt-2"
                    >
                    </li>
                </ul>
            </div>
            <div class="col-12 taskListsFooter text-center align-self-end">
                <hr/> 
                <input type="text" class="form-control form-control-lg d-block w-100" v-model="newTaskList.name"/>
                <b-button @click="addNewTaskList" class="addButtons mt-2">Add new task list</b-button>
            </div>
        </div>
    </div>
</template>

<script>
import TaskList from '../TaskListComponents/TaskList.vue'
import { mapActions, mapGetters } from 'vuex'
export default {
    name: 'TaskListsContainer',
    data: function () {
        return {
            newTaskList: {
                name: null,
                status: "empty"
            },
            options: [
                { value: 'all', text: 'All' },
                { value: 'uncompleted', text: 'Uncompleted tasklists' },
                { value: 'completed', text: 'Completed tasklists' }
            ],
            selectedFilter: 'all',
        }
    },
    computed: {
        // ...mapGetters([
        //     'COMPLETED_TASKLISTS',
        //     'UNCOMPLETED_TASKLISTS'
        // ]),
        ...mapGetters('taskListModule' ,[
            'ALL_TASKLISTS',
            'COMPLETED_TASKLISTS',
            'UNCOMPLETED_TASKLISTS'
        ]),
        filteredTaskLists: function() {
            switch(this.selectedFilter) {
                case 'completed': 
                    return this.COMPLETED_TASKLISTS;
                case 'uncompleted':
                    return this.UNCOMPLETED_TASKLISTS;
                default:
                    return this.ALL_TASKLISTS;
            }
        }
    },
    methods: {
        ...mapActions('taskListModule', [
            'GET_TASKLISTS',
            'ADD_TASKLIST',
            'DELETE_TASKLIST'
        ]),
        ...mapActions('taskModule', [
            'DELETE_ALL_TASKS'
        ]),

        addNewTaskList: function () {
            if(this.newTaskList.name != null) {
                this.ADD_TASKLIST(this.newTaskList);
                this.showInfoMessage('Tasklist added!', `Tasklist "${this.newTaskList.name}" was added!`, 'success');
                this.newTaskList.name = '';
            }
            else {
                this.showInfoMessage('Error!', 'Tasklist name is empty!', 'danger');
            }
        },
        deleteTaskList: function (taskListId, taskListName) {
            this.showConfirmModal(taskListName)
                .then(value => {
                    if(value) {
                        this.DELETE_ALL_TASKS(taskListId);
                        this.DELETE_TASKLIST(taskListId);
                        this.showInfoMessage('Tasklist deleted!', `Tasklist ${taskListName} deleted`, 'success')
                        this.$router.replace('/');
                    }
                })
            
        },

        showInfoMessage: function(title, body, variant) {
            const h = this.$createElement;

            const $vNodesMsg = h(
                'div',
                { class: ['w-100', 'text-center'] },
                [
                    h(
                        'p',
                        body
                    ),
                    h(
                        'b-button',
                        { 
                            on: {click: () => this.$bvToast.hide()},
                            class: ['px-5', `btn-${variant}`]
                        },
                        'OK'
                    )
                ]
            )

            this.$bvToast.toast([$vNodesMsg], {
                title: title,
                variant: variant
            })
        },
        showConfirmModal: function(taskListName) {
            return this.$bvModal.msgBoxConfirm(`Delete the "${taskListName}" tasklist?`, {
                title: 'Delete',
                size: 'sm',
                buttonSize: 'md',
                okTitle: 'Yes',
                cancelTitle: 'Cancel',
                hideHeaderClose: false,
                centered: true
            })
              .then(value => {
                return value;
              })
              .catch(err => {
                  console.log(err);
              })
        }
    },
    mounted: function() {
        this.GET_TASKLISTS();
    },
    components: {
        TaskList
    }
}
</script>

<style lang="scss">
#taskListsContainer {
  border: 2px solid #ddd;
  background-color: #fafafa;
  border-radius: 5px;
  padding: 15px;
  box-shadow: 0 10px 10px 1px rgba(50, 50, 50, .3);
}
#taskListsContainer .row{
    height: 100%;
}
.taskListsHead {
    height: 5%;
}
.taskListsHead {
    .selectFilter {
        text-align: center;
        font-size: 1.3em;
        font-weight: bold;
    }
}
.taskListsFooter {
    .addButtons {
        padding: 8px 20px !important;
    }
}
</style>


